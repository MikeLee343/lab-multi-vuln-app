#!/bin/sh

while getopts "g:l:p:" args
do
    case "${args}" in
        g) sl_app_group=${OPTARG};;
        l) source_lang=${OPTARG};;
        p) working_path=${OPTARG};;
    esac
done

# Echo Environment Variables for Debug
echo "PWD=                        \"$PWD\""  
echo "CI_PROJECT_DIR=             \"$CI_PROJECT_DIR\""
echo "CI_COMMIT_REF_NAME=         \"$CI_COMMIT_REF_NAME\""
echo "SHIFTLEFT_ACCESS_TOKEN      \"$SHIFTLEFT_ACCESS_TOKEN\""
echo "sl_app_group=               \"$sl_app_group\""
echo "source_lang=                \"$source_lang\""
echo "working_path=               \"$working_path\""

# Resolve Language-Specific Scan Variables
if [ "$source_lang" = "js" ] || [ "$source_lang" = "python" ]; then
  source_path="."
elif [ "$source_lang" = "go" ]; then
  source_path="./..."
fi

echo "source_path=                \"$source_path\""

# Initiate Language-Specific Scan
cd $CI_PROJECT_DIR && cd $working_path

sl analyze \
  --app "app-$source_lang" \
  --tag app.group="$sl_app_group" \
  --tag branch="$CI_COMMIT_REF_NAME" \
  --version-id "$CI_COMMIT_SHA" \
  --$source_lang \
  --cpg \
  --oss-recursive \
  --wait \
  $source_path

cd $CI_PROJECT_DIR
