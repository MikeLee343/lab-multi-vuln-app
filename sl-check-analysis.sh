#!/bin/sh

while getopts "a:c:" args
do
    case "${args}" in
        a) app_name=${OPTARG};;
        c) config_path=${OPTARG};;
    esac
done

# Echo Environment Variables for Debug
echo "PWD=                        \"$PWD\""
echo "app_name=                   \"$app_name\""
echo "config_path=                \"$config_path\""

# Resolve Language-Specific Scan Variables
cd $CI_PROJECT_DIR

sl check-analysis \
    --app $app_name \
    --v2 --config $config_path \
    --branch "$CI_COMMIT_REF_NAME" \
    --report-file /tmp/check-analysis.md \
    --source "tag.branch=main" \
    --target "tag.branch=$CI_COMMIT_REF_NAME"
cat /tmp/check-analysis.md
