#!/bin/sh

while getopts "l:p:" args
do
    case "${args}" in
        l) source_lang=${OPTARG};;
        p) working_path=${OPTARG};;
    esac
done

# Echo Environment Variables for Debug
echo "PWD=                        \"$PWD\""
echo "source_lang=                \"$source_lang\""
echo "working_path=               \"$working_path\""

# Initiate Language-Specific Build/Restore
cd $CI_PROJECT_DIR && cd $working_path

if [ "$source_lang" = "python" ]; then
    pip install -r $working_path/requirements.txt
fi

cd $CI_PROJECT_DIR
